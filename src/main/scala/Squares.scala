import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac038 on 2017/5/1.
  */
object Squares extends App{
  val conf = new SparkConf().setAppName("StrLengths").setMaster("local[*]")
  val sc=new SparkContext(conf)

  val intRdd=sc.parallelize(1 to 10000000)

    val squares=intRdd.map(x=>x*x)
  squares.foreach(println)
  squares.collect().foreach(println)

}
