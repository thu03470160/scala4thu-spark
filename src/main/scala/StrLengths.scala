import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac038 on 2017/5/1.
  */
object StrLengths extends App {
  val conf = new SparkConf().setAppName("StrLengths").setMaster("local[*]")
  val sc=new SparkContext(conf)
  val nums=sc.textFile("nums.txt")
  val strLength:RDD[Int]= nums.map(_.length)
  println(strLength)
}
